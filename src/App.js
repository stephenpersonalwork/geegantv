
import React, { Component } from "react";
import { hot } from "react-hot-loader";
import "./App.css";

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="main-header">
          <div className="main-header__svg-container">
          </div>
          <h1 className="main-header__title"> Hello, World! </h1>
        </header>
        <section className="container">
          
        </section>
      </div>
    );
  }
}

export default hot(module)(App);